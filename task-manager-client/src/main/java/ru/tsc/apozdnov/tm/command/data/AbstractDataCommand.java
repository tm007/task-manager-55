package ru.tsc.apozdnov.tm.command.data;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @Autowired
    public IDomainEndpoint domainEndpoint;

}